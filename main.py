import random
import os
import sys

from colorama import initialise
from time import sleep

class Game:

    SLEEP_TIMEOUT = 0.5

    def __init__(self):
        initialise.init()

    def run(self, init_file):
        self.make_first_generation(init_file)
        self.print_generation()

        while self.is_continue_game():
            try:
                sleep(self.SLEEP_TIMEOUT)

                self.make_next_generation()
                self.print_generation()
            except KeyboardInterrupt:
                break

    def is_continue_game(self):
        if len(filter(lambda x: x, self.generation)) == 0:
            return False

        # find periodic sequence
        for prev_generation in self.prev_generations:
            if prev_generation == self.generation:
                return False

        return True

    def make_first_generation(self, init_file):
        self.generation = []
        self.prev_generations = []

        file = open(init_file)
        content = file.read()
        file.close()

        # parse file into generation matrix
        lines = content.split('\n') 
        self.rows = len(lines)

        for line in lines:
            cells = line.split(' ')
            self.cols = len(cells)

            units = [ self.unit_from_string(cell) for cell in cells ]
            self.generation.append(units)

    def print_generation(self):

        # clear screen active region, if any generations have been already printed
        if len(self.prev_generations) > 0:
            print '\033[' + str(self.rows + 1) + 'A'
            sys.stdout.flush()

        # make render matrix from current generation
        matrix = [ 
            [ self.unit_to_string(unit) for unit in row ] 
            for row in self.generation 
        ]

        # do render action
        rows = [ ' '.join(row) for row in matrix ]
        print '\n'.join(rows)
        sys.stdout.flush()

    def make_next_generation(self):

        # calculate next generation
        next_generation = [ row[:] for row in self.generation ]

        for row in range(0, self.rows):
            for col in range(0, self.cols):
                unit = self.generation[row][col]
                neighbors = self.get_neighbors(row, col)

                next_generation[row][col] = self.is_unit_alive(unit, neighbors)

        # store current gen and change current gen to next 
        self.prev_generations.append(self.generation)
        self.generation = next_generation

    def get_neighbors(self, unit_row, unit_col):
        res = []
        
        for row_diff in range(-1, 2):
            for col_diff in range(-1, 2):
                if row_diff == 0 and col_diff == 0:
                    continue

                row = (unit_row + row_diff) % self.rows
                col = (unit_col + col_diff) % self.cols

                neighbor = self.generation[row][col]
                res.append(neighbor)

        return res

    def is_unit_alive(self, unit, neighbors):
        alive_neighbors = filter(lambda x: x, neighbors)
        alive_neighbors_len = len(alive_neighbors)

        if unit:
            return alive_neighbors_len == 2 or alive_neighbors_len == 3
        else:
            return alive_neighbors_len == 3

    def unit_to_string(self, unit):
        return '*' if unit else '.'

    def unit_from_string(self, str):
        return str == '*'

    def get_random_unit(self):
        return random.random() < 0.05


if __name__ == '__main__':
    game = Game()
    game.run('init_gen.txt')